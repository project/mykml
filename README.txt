This module provides a filter to allow users to insert KML files into nodes on top of a google map.

Uses a filter to embed a google map overlayed with a KML file using square brackets
like [mykml:http://mapgadgets.googlepages.com/cta.kml (41.88414, -87.63238), 10] or in general [mykml:path-to-kml-or-georss (lat,lon), zoom]


Install:
- upload to /modules
- enable in admin/settings/modules
- navigate to admin/settings/mykml, to change the Google API key (http://www.google.com/apis/maps/) and to set general layout options. 
- then go to admin/settings/filters and either create a new input format with the MyKML Filter enabled or add the MyKML Filter to "full html". Note: to make this work with the html filter, you'll need to play around with the allowed tags.

Known limitations: 
- by Google's design, kml overlays can only contain 100 map elements
- loads .js files on every page, except admin pages

Send comments to chris@placematters.org